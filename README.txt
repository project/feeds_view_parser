$Id $

A Feeds plugin to import from view (Views) output. The additional value is that sophisticated Views feature combined with Views pluggable query backend in Views 3.x will enable the user to practically import everything from the web into Drupal given that the Views query backend is implemented.

@todo: add example